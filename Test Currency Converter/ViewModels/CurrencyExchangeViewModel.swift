//
//  CurrencyExchangeViewModel.swift
//  Test Currency Converter
//
//  Created by Hakob Hakobyan on 12.07.23.
//

import Foundation

final class CurrencyExchangeViewModel {
    
    typealias CurrencyExchangeCompletionHandler = (CurrencyUIModel?, AlertMessage?) -> Void
        
    private let curencyExchange: CurrencyExchangeNetworkProtocol
    private let transferProtocol: TransferManagerProtocol
    
    init(transferProtocol: TransferManagerProtocol = TransferManager(), curencyExchange: CurrencyExchangeNetworkProtocol = CurrencyExchangeNetworkService()) {
        self.transferProtocol = transferProtocol
        self.curencyExchange = curencyExchange
    }
    
    func exchange(from: Currency, to: Currency, amount: Double, complition: @escaping CurrencyExchangeCompletionHandler) {
        curencyExchange.getExgangeValue(from: from, to: to, value: amount) { response in
            guard let model = response as? CurrencyNetworkModel else { complition(nil, .convertError)
                return }
            print(model)
            complition(CurrencyUIModel.init(model), nil)
        } failure: { error in
            complition(nil, .convertError)
        }
    }
    
    func exchangeWithRate(from: Currency, to: Currency, amount: Double, complition: @escaping CurrencyExchangeCompletionHandler) {
        curencyExchange.getCurentRate(from: from, to: to) { response in
            guard var model = response as? CurrencyNetworkModel else {  complition(nil, .convertError)
                return }
            model.amount = model.safeAmount * amount
            complition(CurrencyUIModel.init(model), nil)
        } failure: { error in
            complition(nil, .convertError)
        }

    }
    
    func fetchBalances(complition: ([CurrencyUIModel]?) -> Void) {
        complition(RealmManager.shared.realm.objects(CurrencyRealmModel.self).map(CurrencyUIModel.init))
    }
    
    func doTransaction(from: CurrencyUIModel, to: CurrencyUIModel) -> Result<AlertMessage, TransferError> {
        return transferProtocol.doTranfer(from: from, to: to)
    }
    
}
