//
//  NetworkManager.swift
//  Test Currency Converter
//
//  Created by Hakob Hakobyan on 30.06.23.
//

import Foundation
import Alamofire

enum NetworkError: Error {
    case successError(message : String)
    case failError(code : Int, message : String)
}

//MARK:- Typealiases
typealias RequestCompletion = (_ response : Any?, _ error : NetworkError?) -> Void
typealias ResponseSuccess = (_ response : Any?) -> Void
typealias ResponseFailure = (_ error : NetworkError?) -> Void

class NetworkManager: NSObject {
    
    func sendRequest(type : HTTPMethod, path : String, pathParameters: [String: Any]? = nil, parameters : [String : Any]? = nil, headers : HTTPHeaders? = nil, timeOut : Double = NetworkTimeout.defaultTimeout, encoding: ParameterEncoding = JSONEncoding.default, comletion : @escaping RequestCompletion)
    {
        
        let alamofireManager = Alamofire.Session.default
        alamofireManager.session.configuration.timeoutIntervalForRequest = timeOut
        
        alamofireManager.request(path, method: type, parameters: parameters, encoding: encoding, headers: headers).validate().responseJSON { response in
            if response.error == nil {
                comletion(response.data, nil)
            } else {
                let errorCode = response.response?.statusCode ?? 0
                let errorMessage = response.error!.localizedDescription
                let failError = NetworkError.failError(code: errorCode, message: errorMessage)
                print("NetworkManager request fail error = \(response.error!) path = \(path)")

                
                comletion(response.data, failError)
            }
        }
    }
}
