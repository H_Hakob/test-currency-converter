//
//  Endpoints.swift
//  Test Currency Converter
//
//  Created by Hakob Hakobyan on 06.07.23.
//

import Foundation
import Alamofire

protocol API {
    static var baseURL: String { get }
}

enum Endpoints {
    enum Convert: API {
        static var baseURL: String = "http://api.evp.lt/"

        case exchange(from: Currency, to: Currency, value: Double)
        
        var path: String {
            switch self {
            case .exchange(let from, let to, let value):
                return Convert.baseURL + "currency/commercial/exchange/\(value)-\(from.rawValue)/\(to.rawValue)/latest"
            }
        }
        
        var method: HTTPMethod {
            switch self {
            case .exchange(_, _, _):
                return .get
            }
        }
    }
    
    enum AlternateConvert: API { // Created for test beacouse endpoint above return time out error https://api.freecurrencyapi.com/v1/latest?apikey=fca_live_3YoFfzPziMaxiG25ExKvkRGvRKkBswn74dqI7qXi&currencies=EUR&base_currency=BGN
        static var baseURL: String = "https://api.freecurrencyapi.com/"
        static let apiKey = "fca_live_3YoFfzPziMaxiG25ExKvkRGvRKkBswn74dqI7qXi"
        
        case exchange(from: Currency, to: Currency)
        
        var path: String {
            switch self {
            case .exchange(let from, let to):
                return AlternateConvert.baseURL + "v1/latest?apikey=\(AlternateConvert.apiKey)&currencies=\(to.rawValue)&base_currency=\(from.rawValue)"
            }
        }
        
        var method: HTTPMethod {
            switch self {
            case .exchange(_, _):
                return .get
            }
        }
    }
}
