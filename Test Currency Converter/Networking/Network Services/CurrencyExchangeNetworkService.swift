//
//  CurrencyExchangeNetworkService.swift
//  Test Currency Converter
//
//  Created by Hakob Hakobyan on 01.07.23.
//

import Foundation

protocol CurrencyExchangeNetworkProtocol {
    func getExgangeValue(from: Currency, to: Currency, value: Double, success: @escaping ResponseSuccess, failure: @escaping ResponseFailure)
    func getCurentRate(from: Currency, to: Currency, success: @escaping ResponseSuccess, failure: @escaping ResponseFailure)
}

final class CurrencyExchangeNetworkService: NetworkManager, CurrencyExchangeNetworkProtocol {
    
    func getExgangeValue(from: Currency, to: Currency, value: Double, success: @escaping ResponseSuccess, failure: @escaping ResponseFailure) {
        
        let endpoint = Endpoints.Convert.exchange(from: from, to: to, value: value)
        
        sendRequest(type: endpoint.method, path: endpoint.path) { response, error in
            guard let _data = response as? Data else { failure(error)
                return }
            do {
                let changedResponse = try JSONDecoder().decode(CurrencyNetworkModel.self, from: _data)
                success(changedResponse)
            } catch let error {
                failure(error as? NetworkError)
            }
        }
    }
    
    func getCurentRate(from: Currency, to: Currency, success: @escaping ResponseSuccess, failure: @escaping ResponseFailure) {
        let endpoint = Endpoints.AlternateConvert.exchange(from: from, to: to)
        
        sendRequest(type: endpoint.method, path: endpoint.path) { response, error in
            guard let _data = response as? Data else { failure(error)
                return }
            do {
                let changedResponse = try JSONDecoder().decode([String:[String: Double]].self, from: _data)
                let currency = Currency(rawValue: changedResponse["data"]?.keys.first ?? "USD")
                let amount = changedResponse["data"]?[currency?.rawValue ?? "USD"]
                success(CurrencyNetworkModel(amount: amount, currency: currency))
            } catch let error {
                failure(error as? NetworkError)
            }
        }
    }
}
