//
//  CurrencyCollectionViewCell.swift
//  Test Currency Converter
//
//  Created by Hakob Hakobyan on 12.07.23.
//

import UIKit

final class CurrencyCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var currencyLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    
    static let reuseIdentifier = "CurrencyCollectionViewCellID"

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func configFrom(currency: CurrencyUIModel) {
        currencyLabel.text = currency.currency?.rawValue
        amountLabel.text = currency.formatet()
    }
    
}
