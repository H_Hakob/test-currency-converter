//
//  ExchangeView.swift
//  Test Currency Converter
//
//  Created by Hakob Hakobyan on 13.07.23.
//

import UIKit

protocol ExchangeViewDelegate: AnyObject {
    func selectCurrency(tag: Int)
    func didWriteAmount(_ amount: Double)
}

enum ExchangeViewMode {
    case sell
    case recive
}

final class ExchangeView: UIView {
    
    let nibName = "ExchangeView"
    var contentView: UIView?
    
    @IBOutlet weak var actionImageView: UIImageView!
    @IBOutlet weak var actionLabel: UILabel!
    @IBOutlet weak var amountTextField: UITextField!
    @IBOutlet weak var currencyLabel: UILabel!
    @IBOutlet weak var optionsArrowImageView: UIImageView!
    @IBOutlet weak var selectCurrencyView: UIView!
    
    weak var delegate: ExchangeViewDelegate?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        guard let view = loadViewFromNib() else { return }
        view.frame = self.bounds
        self.addSubview(view)
        contentView = view
        
        configureGestures()
        connectTextFields()
    }
    
    func loadViewFromNib() -> UIView? {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        return nib.instantiate(withOwner: self, options: nil).first as? UIView
    }
    
    func setMode(_ mode: ExchangeViewMode) {
        switch mode {
        case .sell:
            actionImageView.image = Images.redUpArrow
            actionLabel.text = "Sell"
            amountTextField.textColor = UIColor.darkText
        case .recive:
            actionImageView.image = Images.greenDownArrow
            actionLabel.text = "Recive"
            amountTextField.textColor = UIColor.green
            amountTextField.isUserInteractionEnabled = false
        }
    }
    
    func setCurrency(_ currency: Currency) {
        currencyLabel.text = currency.rawValue
    }
    
    func setChangeAmount(_ currency: CurrencyUIModel) {
        amountTextField.text = currency.formatet(withSimbol: false)
    }
    
    private func configureGestures() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapHandler(_:)))
        
        selectCurrencyView.addGestureRecognizer(tapGesture)
    }
    
    private func connectTextFields() {
        amountTextField.delegate = self
    }
    
    @objc private func tapHandler(_ gesture: UITapGestureRecognizer) {
        delegate?.selectCurrency(tag: self.tag)
    }
}

extension ExchangeView: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if let updatedText = (textField.text as NSString?)?.replacingCharacters(in: range, with: string) {
            let amount = Double(updatedText)
            delegate?.didWriteAmount(amount ?? 0.0)
        }
        return true
        
    }
}
