//
//  CurrencyExchangeViewController.swift
//  Test Currency Converter
//
//  Created by Hakob Hakobyan on 12.07.23.
//

import UIKit

fileprivate enum ExchangeViewType: Int {
    case sellExchangeViewTag = 100
    case receiveExchangeViewTag = 200
}

final class CurrencyExchangeViewController: UIViewController {
    
    @IBOutlet weak var currencyCollectionView: UICollectionView!
    @IBOutlet weak var sellExchangeView: ExchangeView!
    @IBOutlet weak var receiveExchangeView: ExchangeView!
    @IBOutlet weak var submitButton: UIButton!
    
    var viewModel: CurrencyExchangeViewModel!
    
    private var dataSours = [CurrencyUIModel]() {
        didSet {
            currencyCollectionView.reloadData()
        }
    }
    
    private var sellCurrency: Currency = .usd
    private var receiveCurrency: Currency = .eur
    private var sellCurrencyExchangeAmount: Double = 0.0
    private var reciveCurrrencyExchangeAmount: Double = 0.0

    // MARK: - Livecycle funcions
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureUI()
        fetchData()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidLayoutSubviews() {
        configireSubmitButton()
    }
    
    
    private func showActionSheet(options: [String], forView: ExchangeViewType) {
        ActionSheetManager.showActionSheet(in: self, withOptions: options) { selectedOption in
            let selectedCurrency = Currency(rawValue: selectedOption) ?? .usd
            switch forView {
            case .sellExchangeViewTag:
                self.sellExchangeView.setCurrency(selectedCurrency)
                self.sellCurrency = selectedCurrency
            case .receiveExchangeViewTag:
                self.receiveExchangeView.setCurrency(selectedCurrency)
                self.receiveCurrency = selectedCurrency
            }
            
            self.exchangeAction()
        }
    }
    
    @IBAction func submitButtonHandler(_ sender: UIButton) {
        switch viewModel.doTransaction(from: CurrencyUIModel.init(amount: sellCurrencyExchangeAmount, currency: sellCurrency), to: CurrencyUIModel.init(amount: reciveCurrrencyExchangeAmount, currency: receiveCurrency)) {
        case .success(let success):
            fetchData()
            AlertManager.showAlert(success, from: self)
        case .failure(let error):
            switch error {
            case .failError(let error):
                AlertManager.showAlert(error, from: self)
            }
        }
    }
    
}

// MARK: - UI Configuration
private extension CurrencyExchangeViewController {
    func configureUI() {
        configureCollectionView()
        configureExchangeViews()
    }
    
    func configureCollectionView() {
        currencyCollectionView.dataSource = self
        currencyCollectionView.autoresizesSubviews = true
        currencyCollectionView.register(UINib.init(nibName: "CurrencyCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: CurrencyCollectionViewCell.reuseIdentifier)
    }
    
    func configureExchangeViews() {
        sellExchangeView.delegate = self
        receiveExchangeView.delegate = self
        sellExchangeView.tag = ExchangeViewType.sellExchangeViewTag.rawValue
        receiveExchangeView.tag = ExchangeViewType.receiveExchangeViewTag.rawValue
        sellExchangeView.setCurrency(sellCurrency)
        receiveExchangeView.setCurrency(receiveCurrency)
        sellExchangeView.setMode(.sell)
        receiveExchangeView.setMode(.recive)
    }
    
    func configireSubmitButton() {
        submitButton.gradientt(orientation: .horizontal, frame: submitButton.bounds)
        submitButton.roundCorners(corners: .allCorners, radius: submitButton.bounds.height/2)
    }
}

// MARK: - Private methods
private extension CurrencyExchangeViewController {
    func exchangeAction() {
        viewModel.exchangeWithRate(from: sellCurrency, to: receiveCurrency, amount: sellCurrencyExchangeAmount) { [weak self] model, error in
            guard let self else { return }
            
            if let error {
                AlertManager.showAlert(error, from: self)
            }
            
            guard let model else { return }
            self.reciveCurrrencyExchangeAmount = model.safeAmount
            self.receiveExchangeView.setChangeAmount(model)
        }
    }
    
    func fetchData() {
        viewModel.fetchBalances { list in
            dataSours = list ?? []
        }
    }
}

// MARK: - ExchangeViewDelegate
extension CurrencyExchangeViewController: ExchangeViewDelegate {
    func didWriteAmount(_ amount: Double) {
        sellCurrencyExchangeAmount = amount
        exchangeAction()
    }
    
    func selectCurrency(tag: Int) {
        let options = InitialConfigs.initialCurrences
        showActionSheet(options: options, forView: ExchangeViewType(rawValue: tag) ?? .sellExchangeViewTag)
    }
}

// MARK: - UICollectionViewDataSource
extension CurrencyExchangeViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataSours.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CurrencyCollectionViewCell.reuseIdentifier, for: indexPath) as? CurrencyCollectionViewCell else { return UICollectionViewCell() }
        
        cell.configFrom(currency: dataSours[indexPath.row])
        
        return cell
    }
}

