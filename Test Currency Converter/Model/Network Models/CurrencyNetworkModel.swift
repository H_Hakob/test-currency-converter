//
//  CurrencyNetworkModel.swift
//  Test Currency Converter
//
//  Created by Hakob Hakobyan on 01.07.23.
//

import Foundation

struct CurrencyNetworkModel: Codable, CurrencyBaseProtocol {
    var amount: Double?
    var currency: Currency?
}
