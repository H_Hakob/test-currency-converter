//
//  Currency.swift
//  Test Currency Converter
//
//  Created by Hakob Hakobyan on 06.07.23.
//

import Foundation

enum Currency: String, Codable, CaseIterable {
    case usd = "USD"
    case eur = "EUR"
    case jpy = "JPY"
    case chf = "CHF"
    
    var symbol: String {
        switch self {
        case .usd:
            return "$"
        case .eur:
            return "€"
        case .jpy:
            return "¥"
        case .chf:
            return "₣"
        }
    }
}
