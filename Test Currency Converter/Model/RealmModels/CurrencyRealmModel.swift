//
//  CurrencyRealmModel.swift
//  Test Currency Converter
//
//  Created by Hakob Hakobyan on 18.07.23.
//

import Foundation
import RealmSwift

class CurrencyRealmModel: Object {
    
    @objc dynamic var currencyName: String?
    @objc dynamic var amount: Double = 0.0
    
    override static func primaryKey() -> String {
        return "currencyName"
    }
}

extension CurrencyRealmModel {
    convenience init(_ model: CurrencyUIModel) {
        self.init()
        self.currencyName = model.currency?.rawValue
        self.amount = model.safeAmount
    }
}
