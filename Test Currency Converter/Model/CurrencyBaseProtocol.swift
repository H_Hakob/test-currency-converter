//
//  CurrencyBaseProtocol.swift
//  Test Currency Converter
//
//  Created by Hakob Hakobyan on 20.07.23.
//

import Foundation

protocol CurrencyBaseProtocol {
    var amount: Double? { get set }
}

extension CurrencyBaseProtocol {
    var safeAmount: Double {
        amount ?? 0.0
    }
}
