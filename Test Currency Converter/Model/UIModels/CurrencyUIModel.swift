//
//  CurrencyUIModel.swift
//  Test Currency Converter
//
//  Created by Hakob Hakobyan on 16.07.23.
//

import Foundation

struct CurrencyUIModel: Codable, CurrencyBaseProtocol {
    var amount: Double?
    var currency: Currency?
}

extension CurrencyUIModel {
    init(_ model: CurrencyNetworkModel) {
        self.amount = model.amount
        self.currency = model.currency
    }
    
    init(_ object: CurrencyRealmModel) {
        self.amount = object.amount
        self.currency = Currency(rawValue: object.currencyName ?? "USD")
    }
}

extension CurrencyUIModel {
    func formatet(withSimbol: Bool = true) -> String {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .currency
        numberFormatter.currencySymbol = withSimbol ? currency?.symbol : ""
        numberFormatter.locale = Locale.current
        return numberFormatter.string(from: NSNumber(value: safeAmount)) ?? ""
    }
    
    func copy(amount: Double? = nil, currency: Currency? = nil) -> CurrencyUIModel {
        return CurrencyUIModel(amount: amount ?? self.amount, currency: currency ?? self.currency)
    }
}
