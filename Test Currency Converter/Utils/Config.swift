//
//  Config.swift
//  Test Currency Converter
//
//  Created by Hakob Hakobyan on 18.07.23.
//

import Foundation

func saveInitalStateToLocalDB() {
    if RealmManager.shared.realm.objects(CurrencyRealmModel.self).isEmpty {
        RealmManager.shared.createList(InitialConfigs.getInitialData().map(CurrencyRealmModel.init))
    }
}
