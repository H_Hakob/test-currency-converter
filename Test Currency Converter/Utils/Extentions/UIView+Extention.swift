//
//  UIView+Extention.swift
//  Test Currency Converter
//
//  Created by Hakob Hakobyan on 13.07.23.
//

import Foundation
import UIKit

extension UIView {
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
    
    func roundCorners(_ cornerRadius: CGFloat = ViewConstants.defaultCornerRadius, borderColor: UIColor = .clear, borderWidth: CGFloat = 0) {
        self.layer.cornerRadius = cornerRadius
        self.layer.borderColor = borderColor.cgColor
        self.layer.borderWidth = borderWidth
    }
    
    func gradientt(orientation: GradientOrientation, frame: CGRect, cornerRadius: CGFloat = 10, colors: [CGColor] = [Colors.mainBackground.cgColor, Colors.buttonGradien.cgColor] ) {
        for layer in self.layer.sublayers ?? [CALayer()]  {
            if layer.isKind(of: CAGradientLayer.self) {
                layer.removeFromSuperlayer()
            }
        }
        
        let gradientLayer = CAGradientLayer()
        let startPoint: CGPoint
        let endPoint: CGPoint
        
        if orientation == .horizontal {
            startPoint = CGPoint(x: 0.0, y: 0.5)
            endPoint = CGPoint(x: 1.0, y: 0.5)
        } else  {
            startPoint = CGPoint(x: 0.5, y: 0.0)
            endPoint = CGPoint(x: 0.5, y: 1.0)
        }
        gradientLayer.startPoint = startPoint
        gradientLayer.endPoint = endPoint
        
        gradientLayer.frame = frame
        
        gradientLayer.colors = colors
        gradientLayer.cornerRadius = cornerRadius
        self.layer.insertSublayer(gradientLayer, at: 1)
    }
    
    func removeGradientt() {
        for layer in self.layer.sublayers ?? [CALayer()]  {
            if layer.isKind(of: CAGradientLayer.self) {
                layer.removeFromSuperlayer()
            }
        }
    }
}
