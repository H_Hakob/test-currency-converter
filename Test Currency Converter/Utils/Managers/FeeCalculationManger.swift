//
//  FeeCalculationManger.swift
//  Test Currency Converter
//
//  Created by Hakob Hakobyan on 17.07.23.
//

import Foundation

protocol FeeCalculationProtocol {
    func calculateFeeFor(model: CurrencyUIModel) -> CurrencyUIModel
}

final class FeeCalculationManger: FeeCalculationProtocol {
    func calculateFeeFor(model: CurrencyUIModel) -> CurrencyUIModel {
        if UserDefaultsManager.getInt(alias: .transactionCountWithoutFee) < InitialConfigs.initialFreeTrasactionCount {
            return model.copy(amount: 0)
        }
        
        return model.copy(amount: (model.amount ?? 0)*InitialConfigs.initialFeePersentage/100)
    }
}
