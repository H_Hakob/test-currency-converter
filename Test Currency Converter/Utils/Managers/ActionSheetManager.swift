//
//  ActionSheetManager.swift
//  Test Currency Converter
//
//  Created by Hakob Hakobyan on 15.07.23.
//

import Foundation
import UIKit

final class ActionSheetManager {
    typealias OptionHandler = (String) -> Void
    
    static func showActionSheet(in viewController: UIViewController, withOptions options: [String], completionHandler: OptionHandler? = nil) {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        for option in options {
            let action = UIAlertAction(title: option, style: .default) { _ in
                completionHandler?(option)
            }
            alertController.addAction(action)
        }
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertController.addAction(cancel)
        
        viewController.present(alertController, animated: true, completion: nil)
    }
}
