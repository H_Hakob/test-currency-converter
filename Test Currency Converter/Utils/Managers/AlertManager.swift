//
//  AlertManager.swift
//  Test Currency Converter
//
//  Created by Hakob Hakobyan on 15.07.23.
//

import Foundation
import UIKit

enum AlertMessage {
    
    //MARK: - Errors
    case unownedError
    case convertError
    case sameCurrencyError
    case localDataBaseError
    case emptyAmountError
    case insufficientFunds
    
    //MARK: - Success
    case success(to: String, from: String, fee: String)
    
    var message: String {
        switch self {
        case .unownedError:
            return "Something went wrong."
        case .convertError:
            return "Failed to convert."
        case .sameCurrencyError:
            return "Same currency selected, plase change one of them."
        case .localDataBaseError:
            return "Some thing wrong, with local DB."
        case .emptyAmountError:
            return "Amount can't be empty."
        case .insufficientFunds:
            return "Insufficient funds."
        case .success(let to, let from, let fee):
            return "You have converted \(String(describing: from)) to \(String(describing: to)). Commission Fee - \(fee)."
        }
    }
    
    var title: String {
        switch self {
        case .success:
            return "Currency converted"
        default:
            return "Oops!!!"
        }
    }
}

final class AlertManager {
    
    static func showAlert(_ type: AlertMessage, from viewController: UIViewController) {
        showAlert(title: type.title, message: type.message, from: viewController)
    }
    
    
    static func showAlert(title: String, message: String, from viewController: UIViewController, completion: (() -> Void)? = nil) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "OK", style: .default) { _ in
            completion?()
        }
        
        alertController.addAction(okAction)
        
        viewController.present(alertController, animated: true, completion: nil)
    }
}
