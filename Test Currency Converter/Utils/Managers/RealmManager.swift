//
//  RealmManager.swift
//  Test Currency Converter
//
//  Created by Hakob Hakobyan on 16.07.23.
//

import Foundation
import RealmSwift

class RealmManager {
    private init() {}
    static let shared = RealmManager()
    static let currentSchemaVersion: UInt64 = 1
    
    var realm = try! Realm()
    
    func create<T: Object>(_ object: T) {
        do {
            try realm.write {
                realm.add(object, update: .modified)
            }
        } catch let error {
            print("Error: \(error) ")
            post(error)
        }
    }
    
    func createList<T: Object>(_ objects: [T]) {
        do {
            try realm.write {
                for object in objects {
                    realm.add(object, update: .modified)
                }
            }
        } catch let error {
            print("Error: \(error) ")
            post(error)
        }
    }

    func createInBackground<T: Object>(_ object: T, completion: ((Bool) -> Void)? = nil) {
        DispatchQueue.global(qos: .utility).async {
            autoreleasepool {
                let realm = try! Realm()
                realm.beginWrite()
                realm.add(object, update: .modified)
                if let completion = completion {
                    DispatchQueue.main.async {
                        completion(true)
                    }
                }
                print("inrealm")
                try! realm.commitWrite()
            }
        }
    }
    
    func update<T: Object>(_ object: T, with dictionary: [String: Any?]) {
        do {
            try realm.write {
                for (key, value) in dictionary {
                    object.setValue(value, forKey: key)
                }
            }
        } catch let error {
            print("Error: \(error) ")
            post(error)
        }
    }
    
    func updateInBackgrounde<T: Object>(_ object: T, with dictionary: [String: Any?]){
        let ref = ThreadSafeReference(to: object)
        DispatchQueue.global(qos: .utility).async {
            autoreleasepool {
                do {
                    let realm = try Realm()
                    guard let saveObject = realm.resolve(ref) else {
                        return // person was deleted
                    }
                    try realm.write {
                        for (key, value) in dictionary {
                            saveObject.setValue(value, forKey: key)
                        }
                    }
                    print("Bugagagagagagagagagagagagagagagaga!!!!!!!!!!")
                } catch let error {
                    print("Error: \(error) ")
                    self.post(error)
                }
            }
        }
    }
    
    func delete<T: Object>(_ object: T) {
        do {
            try realm.write {
                realm.delete(object)
            }
        } catch let error {
            print("Error: \(error) ")
            post(error)
        }
    }
    
    func deleteList<T: Object>(_ object: [T]) {
        do {
            try realm.write {
                realm.delete(object)
            }
        } catch let error {
            print("Error: \(error) ")
            post(error)
        }
    }
    
    func deleteAll() {
        do {
            try realm.write {
                realm.deleteAll()
            }
        } catch let error {
            print("Error: \(error) ")
            post(error)
        }
    }
    
    func deleteObject(object: Object.Type) {
        do {
            try realm.write {
                realm.delete(realm.objects(object.self))
            }
        } catch let error {
            print("Error: \(error) ")
            post(error)
        }
    }
    
    
    static func configureMigration() {
            
            let config = Realm.Configuration(schemaVersion: currentSchemaVersion, migrationBlock: { (migration, oldSchemaVersion) in
                
                if oldSchemaVersion < 2 {
                    self.migrateFrom1To2(with: migration)
                }
            })
            Realm.Configuration.defaultConfiguration = config
        }
        
    static func migrateFrom1To2(with migration: Migration) {
       // Add migration
    }
    
    
    func post(_ error: Error) {
        NotificationCenter.default.post(name: NSNotification.Name("RealmError"), object: error)
    }
    
    func observeRealmErrors(in vc: UIViewController, completion: @escaping (Error?) -> Void) {
        NotificationCenter.default.addObserver(forName: NSNotification.Name("RealmError"),
                                               object: nil,
                                               queue: nil) { (notification) in
                                                completion(notification.object as? Error)
        }
    }
    
    func stopObservingErrors(in vc: UIViewController) {
        NotificationCenter.default.removeObserver(vc, name: NSNotification.Name("RealmError"), object: nil)
    }
}
