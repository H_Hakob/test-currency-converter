//
//  TransferManger.swift
//  Test Currency Converter
//
//  Created by Hakob Hakobyan on 18.07.23.
//

import Foundation
import RealmSwift

enum TransferError: Error {
    case failError(error: AlertMessage)
}

protocol TransferManagerProtocol {
    func doTranfer(from: CurrencyUIModel, to: CurrencyUIModel) -> Result<AlertMessage, TransferError>
}

final class TransferManager: TransferManagerProtocol {
    
    let feeCalculationProtocol: FeeCalculationProtocol
    init(feeCalculationProtocol: FeeCalculationProtocol = FeeCalculationManger()) {
        self.feeCalculationProtocol = feeCalculationProtocol
    }
    
    func doTranfer(from: CurrencyUIModel, to: CurrencyUIModel) -> Result<AlertMessage, TransferError> {
        if from.safeAmount <= 0 || to.safeAmount <= 0 {
            return .failure(.failError(error: .emptyAmountError))
        }
        
        guard let fromCurrencyObject = fetchBalanceForCurrency(from.currency ?? .usd), let toCurrencyObject = fetchBalanceForCurrency(to.currency ?? .usd) else { return .failure(.failError(error: .localDataBaseError)) }
        let fee = returnFee(from)
        if fromCurrencyObject.amount < (from.safeAmount + fee.safeAmount) {
            return .failure(.failError(error: .insufficientFunds))
        }
        
        let reciveAmount = toCurrencyObject.amount + to.safeAmount
        let sellAmount = fromCurrencyObject.amount - (from.safeAmount + fee.safeAmount)
        
        RealmManager.shared.update(toCurrencyObject, with: ["amount" : reciveAmount])
        RealmManager.shared.update(fromCurrencyObject, with: ["amount" : sellAmount])
        
        let transactionCount = UserDefaultsManager.getInt(alias: .transactionCountWithoutFee) + 1
        UserDefaultsManager.set(alias: .transactionCountWithoutFee, value: transactionCount)
        
        return .success(.success(to: to.formatet(), from: from.formatet(), fee: fee.formatet()))
    }
}

private extension TransferManager {
    func returnFee(_ model: CurrencyUIModel) -> CurrencyUIModel {
        feeCalculationProtocol.calculateFeeFor(model: model)
    }
    
    func fetchBalanceForCurrency(_ currency: Currency) -> CurrencyRealmModel? {
        return RealmManager.shared.realm.object(ofType: CurrencyRealmModel.self, forPrimaryKey: currency.rawValue)
    }
}

