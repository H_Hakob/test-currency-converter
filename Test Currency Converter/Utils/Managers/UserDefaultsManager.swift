//
//  UserDefaultsManager.swift
//  Test Currency Converter
//
//  Created by Hakob Hakobyan on 17.07.23.
//

import Foundation

enum UDAliases: String {
    case firstAppOpen
    case transactionCountWithoutFee
}


final class UserDefaultsManager {
    
    static func isNil(_ alias: UDAliases) -> Bool {
        return UserDefaults.standard.string(forKey: alias.rawValue) == nil
    }
    
    static func getString(for alias: UDAliases) -> String? {
        return UserDefaults.standard.string(forKey: alias.rawValue)
    }
    
    static func get(alias: UDAliases) -> Any? {
        return UserDefaults.standard.string(forKey: alias.rawValue)
    }
    
    static func getData(alias: UDAliases) -> Data? {
        return  UserDefaults.standard.data(forKey: alias.rawValue)
    }
    
    static func getValue(alias: UDAliases) -> Any? {
        return UserDefaults.standard.value(forKey: alias.rawValue)
    }
    
    static func getDict(alias: UDAliases) -> [String: Any]? {
         return UserDefaults.standard.dictionary(forKey: alias.rawValue)
    }
    
    static func getArray(alias: UDAliases) -> [AnyObject]? {
        return UserDefaults.standard.array(forKey: alias.rawValue) as [AnyObject]?
    }
    
    static func getInt(alias: UDAliases) -> Int {
        return UserDefaults.standard.integer(forKey: alias.rawValue)
    }
    
    static func boolFor(alias: UDAliases) -> Bool? {
        return UserDefaults.standard.bool(forKey: alias.rawValue)
    }
    
    static func set(alias: UDAliases, value: Int) {
        UserDefaults.standard.set(value, forKey: alias.rawValue)
    }
    
    static func set(alias: UDAliases, value: String) {
        UserDefaults.standard.set(value, forKey: alias.rawValue)
    }
    
    static func set(alias: UDAliases, value: Any?) {
        UserDefaults.standard.set(value, forKey: alias.rawValue)
    }
    
    static func remove(alias: UDAliases) {
        UserDefaults.standard.removeObject(forKey: alias.rawValue)
    }
    
    static func remove(aliases: UDAliases...) {
        aliases.forEach { (alias) in
            UserDefaults.standard.removeObject(forKey: alias.rawValue)
        }
    }
}
