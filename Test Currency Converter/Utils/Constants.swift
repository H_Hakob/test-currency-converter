//
//  Constants.swift
//  Test Currency Converter
//
//  Created by Hakob Hakobyan on 01.07.23.
//

import Foundation

enum GradientOrientation {
    case horizontal
    case vertical
}

struct NetworkTimeout {
    static let defaultTimeout: Double = 2
}

struct InitialConfigs {
    static let initialCurrences = Currency.allCases.map { $0.rawValue }
    static let initialAmount = 1000.0
    static let initialFreeTrasactionCount = 5
    static let initialFeePersentage = 0.7
    
    static func getInitialData() -> [CurrencyUIModel] {
        var result = [CurrencyUIModel]()
        for item in initialCurrences {
            var model = CurrencyUIModel(amount: 0.0, currency: Currency(rawValue: item))
            if item == Currency.usd.rawValue {
                model.amount = 1000.00
            }
            result.append(model)
        }
        return result
    }
}

struct ViewConstants {
    static let defaultCornerRadius: CGFloat = 10.0
}
