//
//  Colors.swift
//  Test Currency Converter
//
//  Created by Hakob Hakobyan on 13.07.23.
//

import Foundation
import UIKit

struct Colors {
    static let buttonGradien = UIColor(named: "ButtonGradien")!
    static let mainBackground = UIColor(named: "MainBackground")!
    static let separator = UIColor(named: "SeparatorColor")!
}
