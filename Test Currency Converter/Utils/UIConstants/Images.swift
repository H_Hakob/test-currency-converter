//
//  Images.swift
//  Test Currency Converter
//
//  Created by Hakob Hakobyan on 13.07.23.
//

import Foundation
import UIKit

struct Images {
    static let closeOptionsArrow = UIImage(named: "CloseOptionsArrow")!
    static let greenDownArrow = UIImage(named: "GreenDownArrow")!
    static let openOptionsArrow = UIImage(named: "OpenOptionsArrow")!
    static let redUpArrow = UIImage(named: "RedUpArrow")!
}
